#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-

# We will disallow to fail on unstable/sid/UNRELEASED, but let them pass otherwise
# TODO:  return back exit 1 upon failures
include /usr/share/dpkg/default.mk
failed_tests_cmd = $(shell dpkg-parsechangelog --count 1 --format rfc822 | awk '/^Distribution:/{print $$2;}' | grep -q -e '\(sid\|unstable\|UNRELEASED\)' && echo ":" || echo ":")

PYVER = $(shell py3versions -vd)

export DEBPYTHON_SUPPORTED=$(PYVER)
export HOME=$(CURDIR)/build
DEB_DESTDIR=$(CURDIR)/debian/psychopy
XVFB_RUN=xvfb-run -s "-screen 0 1024x768x24 -ac +extension GLX +render -noreset"

# Avoid any network interactions
export http_proxy=http://127.0.0.1:9/
export https_proxy=http://127.0.0.1:9/


%:
	dh $@ --buildsystem pybuild

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	echo "I: The test suite is not compatible with pytest > 4.0"
	echo "   See https://discourse.psychopy.org/t/how-to-run-unit-tests/7315/7"
	find . -name psychopy -type f -executable
	USER=nobody \
	TRAVIS=true \
		$(XVFB_RUN) python3 \
			-m pytest \
			--tb=line -s -v \
			-m "not needs_sound" \
			$(CURDIR)/psychopy/tests \
	|| { echo "TESTS FAILED"; $(failed_tests_cmd); }
	# nosetest does not change anything
	# LC_ALL=C.UTF-8 PYTHONPATH=$PWD xvfb-run -s "-screen 0 1280x1024x24 -noreset" nosetests -s -v psychopy/tests 2>&1
endif

override_dh_auto_install:
	dh_auto_install
## Fix up permissions on images -- pushed upstream, but left here
## just to make sure everything is ok
	find $(DEB_DESTDIR) -name \*.png -o -name \*.ico -o -name \*.jpg -executable -exec chmod -x \{\} \;
	find debian -name packaging -type d | xargs rm -rf

## convert and install icon's .xpm manually
	convert psychopy/app/Resources/psychopy.png -geometry 32x32 \
      $(DEB_DESTDIR)/usr/share/pixmaps/psychopy.xpm
## Rename upstream's runner FIXME: runner ended up in /usr/lib
	# mv $(DEB_DESTDIR)/usr/bin/psychopyApp.py $(DEB_DESTDIR)/usr/bin/psychopy

debian/psychopy.1: debian/psychopy.sh
	PYTHONPATH=. $(XVFB_RUN) help2man -N -n "Creating psychology stimuli in Python" $^ > $@
	sed -i 's/^\.TH PSYCHOPY3, /.TH PSYCHOPY /' $@
## cleanup
	find debian -name __pycache__ -type d | xargs rm -rf

override_dh_link:
	dh_link -ppsychopy /usr/lib/python3/dist-packages/psychopy/demos usr/share/doc/psychopy/examples/demos
	dh_link

override_dh_installman:  debian/psychopy.1
	dh_installman

override_dh_installchangelogs:
	dh_installchangelogs psychopy/CHANGELOG.txt

override_dh_clean::
	rm -f debian/psychopy.1
	rm -rf build dist PsychoPy.egg-info
	dh_clean

# do not actually install demos -- just symlink to them
#DEB_INSTALL_EXAMPLES_psychopy := psychopy/demos/*
